import time
from selenium import webdriver
import pytest

# Fixture for Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture(scope="class")
def driver_init(request):
    driver = webdriver.Firefox(executable_path='C:/Tools/geckodriver.exe')
    request.cls.driver = driver
    driver.implicitly_wait(1)
    driver.maximize_window()
    # driver.get(mega_ru)
    # driver.add_cookie({'domain': 'mega.ru', 'expiry': 1609654126, 'httpOnly': False, 'name': 'showed_NY_popup', 'path': '/','secure': False, 'value': 'false'})
    # driver.add_cookie({'domain': '.mega.ru', 'httpOnly': False, 'name': 'geo', 'path': '/', 'secure': False, 'value': 'belaya_dacha'})
    yield
    driver.close()
    print("Browser closed")


mega_ru = "https://mega.ru"
mega_ru_404 = "https://mega.ru/hggfdj"

mega_ru_prod = "https://mega.ru"
mega_ru_404_prod = "https://mega.ru/hggfdj"

@pytest.mark.usefixtures("driver_init")
class Test_Class_01():


    # ТС_01: Переход на не существующую страницу «404»
    def test_TC_01(self):
        driver = self.driver
        driver.get(mega_ru_404)
        time.sleep(1)

    # ТС_02: Клик на «главную» на странице «404»
    def test_TC_02(self):
        driver = self.driver
        driver.get(mega_ru_404)
        driver.find_element_by_link_text(u"главную").click()